#include<stdio.h>

int partition(int a[], int low, int high);
void swap(int *a, int *b){
	printf("\n\n\nswap(array , %d, %d)", *a,*b);
	int t = *a;
	*a = *b;
	*b = t;	
}

void quicksort(int a[],int low,int high){
	printf("\n\n quicksort()");
	int p;
	if(low<high){
	p = partition(a,low,high);
	quicksort(a,low,p-1);quicksort(a,p+1,high);
	}
}
int partition(int a[], int low, int high){
	printf("\n\npartition()");
	int pivot = a[low];
	int i=low,j=high+1;
	do{
		do{
			i++;
		}while(a[i]>pivot);
		do{
			j--;
		}while(a[j]<=pivot);
		swap(&a[i],&a[j]);
	}while(j<i);
	swap(&a[j],&a[low]);
	return(low);
}

int main(){
	printf("\n\n\nmain()");
	int a[10], n,i;
	printf("\t\t___ QUICK SORT ___\n\n\nEnter the limit of the array: ");scanf("%d",&n);
	printf("\n\n\n___ ENTER THE ELEMENTS OF THE ARRAY ___");
	for(i = 0; i<n; i++){
		printf("a[%d] ==> ",i);scanf("%d", &a[i]);
	}
	quicksort(a,0,n-1);
	printf("\n\n___THE SORTED ARRAY __ \n\n\n");
	for(i = 0; i < n ; i++){
		printf("==> %d",a[i]);
	}	

}
