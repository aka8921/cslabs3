#include<stdio.h>
#include<stdlib.h>
typedef struct node NODE;
struct node{
	int data;
	struct node *next;
}*start = NULL ;
void create(){
	int decision = 1;
	do{
		NODE *newnode, *current;
		newnode = (NODE *)malloc(sizeof(NODE));
		printf("enter the data: ");
		scanf("%d", &newnode  -> data);
		if (start == NULL){
			start = newnode;
			current = newnode;
		}
		else{
			current -> next = newnode;
			current = newnode;
		}
		printf("do you want to continue? (0 --Yes, 1 --False): : :");
		scanf("%d", &decision);
	}while(decision != 1);
}

void traverse(){
	NODE *trv;
	if(start == NULL){
		printf("empty linked list");
		exit(0);
	}
	trv = start;
	do{
		printf("=>  %d \n", trv -> data);
		trv = trv -> next;
		
	}while(trv != NULL );
}

void search(int el){
	NODE *current = start;
	int c = 0,f = 0;
	if(start != NULL) 
	do{
		c++;
		if(current -> data == el){printf("element found at node %d \n", c);f=1;break;}
		current = current -> next;
	}while(current != NULL);
	if(f == 0)printf("element not found");
}

void insert(c){
		int count = 1;
		NODE * newnode,*current = start; 
		newnode = (NODE *) malloc(sizeof(NODE));
		printf("enter the data : ");scanf("%d", &newnode -> data);
		if(c == 1){
			printf("inserting in the first position \n");
			newnode -> next = start;
			start = newnode;
			return;
		}
		else{
			while(current != NULL)
			{
			
				count++;
				if(c == count){
					newnode->next = current->next;
					current->next = newnode;
					return;
					}
				current = current->next;
			}
		}
		printf("can't insert in the given position");
}

void delete(c){
	int count = 1;
	NODE *current = start,*temp;
	if(c == 1){
		current = current->next;
		free(start);
		start = current;
		return;
	}
	else{
		while(current->next != NULL){
			count++;
			if(c == count){
				temp = current->next;
				current->next = temp->next;
				free(temp);
				return;
			}
			current = current->next;
		}
	printf("the given position is out of scope");
	}	
}
void main(){
	int el,pos,option,decision;
	create();
	traverse();
	while(1){
	printf("enter the operation to be performed \n 0 ==> traverse\n1 ==> search\n 2 ==> insert\n 3  ==> delete\n");
	scanf("%d",&option);
	switch(option){
	case 0: traverse();break;
	case 1: printf("enter the element to search : ");
		scanf("%d", &el);
		search(el);
		break;
	case 2: printf("enter the position to insert");
		scanf("%d", &pos);
		insert(pos);
		traverse();
	case 3: printf("enter the position to delete : ");
		scanf("%d", &pos);
		delete(pos);
		traverse();
		break;
	default: printf("input is not valid \n");
	}
	printf("do you want to continue ( 0 ==> yes, 1 ==> no)?");scanf("%d",&decision);
	if(decision == 1){break;}
}
}
