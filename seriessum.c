#include<stdio.h>

int fact(int n){
	if (n == 1 || n == 0)return 1;
	else return(n * fact(n-1));
}

int main(){
	int n, i = 1;
	float sum = 0;
	printf("enter the value of n: ");scanf("%d", &n);
	for (i = 1; i <= n; i++)
		sum += (1.0 / fact(i));  
	printf("the sum of the series is : %f", sum);
}
