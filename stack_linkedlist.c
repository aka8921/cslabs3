#include<stdio.h>
#include<stdlib.h>
typedef struct node NODE;
struct node{
	int data;
	struct node *next;
} *top = NULL;
void push(){
	NODE* newnode;
	newnode  = (NODE *)malloc(sizeof(NODE));
	int el;
	printf("enter the element to push: ");scanf("%d",&el );
	if (top == NULL){top = newnode;top->data = el;return;}
	newnode->next = top; newnode->data = el;
	top = newnode; 
	
}
void pop(){
	NODE * temp = top;
	if(top == NULL){printf("\n \t\tcan't pop, \n\n\n===================> STACK UNDERFLOW <=====================");return;}
	top = top->next;free(temp);
}
void traverse(){
	NODE *current = top;
	while ( current != NULL ){
		printf("\n  =====> %d", current->data);
		current = current->next;
	}
}
int main(){
	//menu driven
	int i=0, choice, cont = 0;
	while(1){
	printf("choose your option \n \n1==> PUSH\n2==> POP\n3==> TRAVERSE\n\n ===> ");scanf("%d", &choice);
	switch(choice){
		case 1: push();break;
		case 2: pop();break;
		case 3: traverse();break;
		default: printf("wrong input");
	}
	printf("\ndo you want to continue ? (1/0)");scanf("%d", &cont);
	if(cont == 0)break;
	}
}
